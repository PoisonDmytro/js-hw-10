//--------------
giveId();
//-------------
tabs_list.onclick = function(event){
    let old_active = document.querySelector('.active')
    let old_content = document.querySelector('.active_content')
    let target_element = event.target;
    let contentId = `${target_element.id}_content`;
    
    if(old_active != null){
        old_active.classList.remove('active');
        old_content.classList.remove('active_content')
    }
    target_element.classList.add('active');
    document.querySelector(`#${contentId}`).classList.add('active_content')

}
function giveId() {
    let tabsLi_list = document.querySelectorAll('#tabs_list > li');
    for(tabs of tabsLi_list){
        tabs.id = `${tabs.textContent}`;
    }
    let contenList = document.querySelectorAll('.tabs-content > li');
    let contentCounter = 0;
    for(content of contenList){
        content.id = `${tabsLi_list[contentCounter].id}_content`
        content.classList.add('hiden')
        contentCounter++;
    }
}